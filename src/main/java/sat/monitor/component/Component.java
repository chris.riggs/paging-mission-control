package sat.monitor.component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonObject;

/* Entry
 * This class is used to hold data for each entry processed from the input
 *     data.  It also calculates any alert states on its own data based on
 *     which component it represents.  This would have been done in different
 *     "component" classes, but at this point the behaviors are so similar
 *     that it wasn't really worth different classes.
 */
public class Component {

    // Static attributes used for status determination and externally by the
    // application to determine when to send out alerts.
    public static final String BATTERY = "BATT";
    public static final String TEMPERATURE = "TSTAT";
    public static final int SAFE = 0;
    public static final int ALERT = 1;

    // Expected format object for LocalDateTime field timestamp
    private static final DateTimeFormatter inputDateTimeFormat =
            DateTimeFormatter.ofPattern("yyyyMMdd' 'HH:mm:ss.SSS");

    // Output format for alert timestamp
    private static final DateTimeFormatter alertDateTimeFormat =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    // Attributes assigned at creation of the object
    private LocalDateTime timestamp;
    private int satId;
    private float redHighLimit;
    private float yellowHighLimit;
    private float yellowLowLimit;
    private float redLowLimit;
    private float rawValue;
    private String component;

    /* Component
     * This is the main constructor for the Component class.
     *     It takes a String line as input to initialize the individual fields.
     *     The fields are converted to local types on initialization for future
     *     use.
     */
    public Component(String line) {

        // Convert and assign based on position in the input string
        String[] fields = line.split("\\|");
        this.timestamp = LocalDateTime.parse(fields[0], inputDateTimeFormat);
        this.satId = Integer.parseInt(fields[1]);
        this.redHighLimit = Float.parseFloat(fields[2]);
        this.yellowHighLimit = Float.parseFloat(fields[3]);
        this.yellowLowLimit = Float.parseFloat(fields[4]);
        this.redLowLimit = Float.parseFloat(fields[5]);
        this.rawValue = Float.parseFloat(fields[6]);
        this.component = fields[7];
    }

    /* checkStatus
     * This method is used to determine if the status of this data line is an
     *     alert or not.  This behavior is the only real difference between
     *     the two component types currently.
     */
    public int checkStatus() {
        int retVal = SAFE;

        // If the component is a battery, the check is to make sure it's not
        // too low, below the red low limit.
        if (this.component.equals(BATTERY)) {
            if (this.rawValue < this.redLowLimit)
                retVal = ALERT;

        // This block covers the temperature value check.  It is an alert
        // only if the value is above the red high level.
        } else {
            if (this.rawValue > this.redHighLimit)
                retVal = ALERT;
        }
        return retVal;
    }

    /* getAlertJson
     * This method is used to return a string representation of the alert data
     *     in JSON format.  The GSON version of JsonObject was used because it
     *     seems to keep the order of the properties that are added to the
     *     object when using .toString() on the JsonObject.
     */
    public JsonObject getAlertJson() {
        // Using a GSON JsonObject to build up the alert information
        JsonObject json = new JsonObject();
        json.addProperty("satelliteId", this.satId);

        // The severity value depends on which component this is for
        if (this.component.equals(BATTERY))
            json.addProperty("severity", "RED LOW");
        else
            json.addProperty("severity", "RED HIGH");

        json.addProperty("component", this.component);
        json.addProperty("timestamp", this.timestamp.format(alertDateTimeFormat));

        return json;
    }

    // Standard getters for the necessary attributes below
    //
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public int getSatId() {
        return satId;
    }

    public String getComponent() {
        return component;
    }
}
