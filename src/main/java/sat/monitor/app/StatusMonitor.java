package sat.monitor.app;

import sat.monitor.component.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

/* StatusMonitor
 * This class monitors the status (read in from a file passed as an argument)
 *     of the components of satellite systems and outputs alerts for either low
 *     battery or high temperature.  An alert is only output if three alarms
 *     beyond a given threshold come in for the same satellite system within
 *     five minutes of each other.
 */
public class StatusMonitor {

    // Maps to hold the ArrayLists associated with each satellite
    // There is a map for each of the components.
    private Map<Integer,ArrayList<Component>> batterySatMap;
    private Map<Integer,ArrayList<Component>> tempSatMap;

    // JsonArray to be used for output
    private JsonArray jArray;

    // File reader
    private Scanner myReader;

    /* StatusMonitor
     * Constructor used to verify input file exists before continuing.  Also
     *     initializes maps and file reader for later use.
     */
    public StatusMonitor(String dataFileName) throws FileNotFoundException {
        File inputFile = new File(dataFileName);
        myReader = new Scanner(inputFile);
        batterySatMap = new HashMap<>();
        tempSatMap = new HashMap<>();
        jArray = new JsonArray();
    }

    /* processAlert
     * This method is used to perform the necessary logic associated with an
     *     alert status for a component.  If no prior alerts have been
     *     processed for this component and satelliteId, a new list is created
     *     to hold the alerts for that component and satelliteId.  Otherwise
     *     the existing list is retrieved from the component map.  In either
     *     case, the alert is added to the retrieved or created list.  A check
     *     is then performed to see if at least three components have been
     *     processed and added to this list.  If three have already been
     *     processed, the oldest alert is retrieved.  A check is made to
     *     determine if the most recent alert is within five minutes of the
     *     oldest alert.  If this is the case, an alert message is output in
     *     JSON format.
     */
    private void processAlert(Component alert, Map compMap) {
        ArrayList<Component> compList;

        // If the map has an entry list for this satelliteId already this list
        // is used
        if (compMap.containsKey(Integer.valueOf(alert.getSatId()))) {
            // Obtain the list for this entry's satelliteId and add this entry
            compList = (ArrayList<Component>) compMap.get(Integer.valueOf(
                    alert.getSatId()));
            compList.add(alert);

            // Only take further action if there are at least 3 entry objects
            // in this list
            if (compList.size() > 2) {
                Component oldestEntry = compList.get(0);

                // Check to see if the newest entry is within 5 minutes of the
                // oldest one. If so, add the oldest entry to the output array.
                if (oldestEntry.getTimestamp().plusMinutes(5)
                        .isAfter(alert.getTimestamp())) {
                    jArray.add(oldestEntry.getAlertJson());
                }

                // Remove the oldest entry whether or not it was output as an
                // alert.  This is to ensure it is not alerted on again.  Also,
                // if the alert wasn't output due to the five minute time span
                // having expired, this should still be removed for that reson.
                // Leaving the other alerts in the list in case a new alert
                // comes in within the new five minute span of time. This is to
                // ensure ongoing problems get alerted on visibly so that
                // personnel monitoring the system alerts easily see the issue
                // is ongoing.
                compList.remove(oldestEntry);
            }
        }
        // If there is no entry list for this satelliteId one needs to be
        // created. The new entry is then added to the list, and the list is
        // put into the map.
        else {
            compList = new ArrayList<>();
            compList.add(alert);
            compMap.put(Integer.valueOf(alert.getSatId()), compList);
        }
    }

    /* startMonitoring
     * This method is the primary driver of the monitoring and alerting
     *     process.  It reads the input file line by line, processing as it
     *     goes.  This is more like a streaming system input in the way things
     *     are processed.
     */
    public void startMonitoring() {
        while (myReader.hasNextLine()) {
            // Read line and create a Component status object
            String line = myReader.nextLine();
            Component comp = new Component(line);

            // If the line represents an alert status, perform extra processing
            if (comp.checkStatus() == Component.ALERT) {

                // Choose the map to use based on the component type
                if (comp.getComponent().equals(Component.BATTERY))
                    processAlert(comp, batterySatMap);
                else
                    processAlert(comp, tempSatMap);
            }
        }
        // GsonBuilder allows the setting of "pretty printing" to automatically
        // add the white space and new lines for visual JSON formatting
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(jArray));
    }

    /* main
     * This is the entry point of the StatusMonitor application.
     * It takes a filename as input to be read in.
     */
    public static void main(String[] args) {
        String filename = null; // = "src\\main\\resources\\input_data.txt";
        try {
            if (args.length > 0)
                filename = args[0];
            StatusMonitor statMon = new StatusMonitor(filename);
            statMon.startMonitoring();
        } catch (FileNotFoundException e) {
            System.out.println("File (" + filename + ") does not exist.  Please use a valid file.");
            System.out.println("Usage from the main project directory is:\n");
            System.out.println("\tgradlew run --args src\\main\\resources\\input_data.txt");
            System.out.println("\nOther files may be used in place of this sample file.\n\n");
            e.printStackTrace();
        }
    }
}


