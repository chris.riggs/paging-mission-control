package sat.monitor;

import org.junit.Test;
import sat.monitor.component.Component;

import static org.junit.Assert.*;

import com.google.gson.JsonObject;

public class ComponentTest {

    private static final String GOOD_TEMP = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
    private static final String BAD_BATT = "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT";
    private static final String BAD_TEMP = "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT";

    // These variables will house the JsonObuect data to compare to the object
    // being returned by the component's getAlertJson() method
    private JsonObject BAD_BATT_JSON;
    private JsonObject BAD_TEMP_JSON;


    @Test public void testComponent() {
        Component classUnderTest = new Component(GOOD_TEMP);
        assertNotNull("Verifying GOOD_TEMP Component object created", classUnderTest);
        assertEquals(1001, classUnderTest.getSatId());
        assertEquals(Component.TEMPERATURE, classUnderTest.getComponent());
        assertEquals("2018-01-01T23:01:05.001", classUnderTest.getTimestamp().toString());
        assertEquals(Component.SAFE, classUnderTest.checkStatus());

        // Build up the bad battery JsonObject
        BAD_BATT_JSON = new JsonObject();
        BAD_BATT_JSON.addProperty("satelliteId", 1000);
        BAD_BATT_JSON.addProperty("severity", "RED LOW");
        BAD_BATT_JSON.addProperty("component", "BATT");
        BAD_BATT_JSON.addProperty("timestamp", "2018-01-01T23:01:09.521Z");

        classUnderTest = new Component(BAD_BATT);
        assertNotNull("Verifying BAD_BATT Component object created", classUnderTest);
        assertEquals(1000, classUnderTest.getSatId());
        assertEquals(Component.BATTERY, classUnderTest.getComponent());
        assertEquals("2018-01-01T23:01:09.521", classUnderTest.getTimestamp().toString());
        assertEquals(Component.ALERT, classUnderTest.checkStatus());
        JsonObject json = classUnderTest.getAlertJson();
        assertEquals(BAD_BATT_JSON.toString(), json.toString());

        // Build up the bad temperature JsonObject
        BAD_TEMP_JSON = new JsonObject();
        BAD_TEMP_JSON.addProperty("satelliteId", 1000);
        BAD_TEMP_JSON.addProperty("severity", "RED HIGH");
        BAD_TEMP_JSON.addProperty("component", "TSTAT");
        BAD_TEMP_JSON.addProperty("timestamp", "2018-01-01T23:03:03.008Z");

        classUnderTest = new Component(BAD_TEMP);
        assertNotNull("Verifying BAD_TEMP Component object created", classUnderTest);
        assertEquals(1000, classUnderTest.getSatId());
        assertEquals(Component.TEMPERATURE, classUnderTest.getComponent());
        assertEquals("2018-01-01T23:03:03.008", classUnderTest.getTimestamp().toString());
        assertEquals(Component.ALERT, classUnderTest.checkStatus());
        json = classUnderTest.getAlertJson();
        assertEquals(BAD_TEMP_JSON.toString(), json.toString());
    }
}
