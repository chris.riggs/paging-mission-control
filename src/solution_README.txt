Paging Mission Control Challenge

This is a short guide for my solution to the Paging Mission Control challenge
problem.  Most of my comments can be found within the class files.  They will
describe what the logic is doing and/or why that bit of code was chosen for the
sections they are above or within.  As an example, in Component.java I mention
why I left that as one class instead of breaking it up into Battery and
Temperature classes.

The work was performed in IntelliJ IDEA, mostly because I wanted to learn a bit
about that IDE while I was at it.  The build is performed in gradle, so the
following commands should be run on the command line of the main project
directory (paging-mission-control/).


gradlew clean build

This builds the project and runs the two tests, StatusMonitorTest and
ComponentTest.  I left a basic call to my main startMonitoring() method in the
StatusMonitorTest to show the application does not throw an exception.


gradlew run --args src/main/resources/input_data.txt

This command runs the application with the default input_data.txt file.  This
file contains the contents of the sample input given in the problem README.txt
file.  Other files with the same formatting can be used in place of this file
if desired.


I will be in touch with Jaime to let her know I've submitted this.

Thank you for the opportunity to try this out.  It was fun.
